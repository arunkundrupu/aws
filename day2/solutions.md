

#Created a vpc manually

![aws](https://github.com/arunkundrupu1990/AWS/blob/master/day2-images/01%20vpc%20manual.png)

Created 2 public subnets and 2 private subnets and 2 protected subnets within that vpc.

![aws](https://github.com/arunkundrupu1990/AWS/blob/master/day2-images/02%20subnets.png)

#Created 1 IGW and attached to two public subnets

![aws](https://github.com/arunkundrupu1990/AWS/blob/master/day2-images/03%20IGW.png)

#Created 2 NGW in each availability zone

![aws](https://github.com/arunkundrupu1990/AWS/blob/master/day2-images/04%20RT.png)

# Main route will remain intact, and created 4 route tables

    pub RT

    pri RT-1

    pri RT-2

    pro RT

![aws](https://github.com/arunkundrupu1990/AWS/blob/master/day2-images/05%20nat%20gateway.png)

IAM:

Identity and Access Mnagement is a web service that help&#39;s secure control access to aws resource for your user.

Componets of IAM:

There are mainly 4 components in IAM. They are :

User,  Group,  Roles, &amp; Policies

User :- Using IAM we can create and manage users and user perimissions to allow and deny their access to AWS resource.

# Created users

Group :- Within sepcified group we can add users and then the rules and policies can apply on the group and users level as well.

#Created groups

Roles :- An IAM role is an IAM entity that defines a set of perimissions for making aws service requests.

IAM roles are not associated with a specific user or group instead, trusted entites assume roles, such as IAM user application or AWS service such as ec2.

Policies :- To assign perimission to a user, group, role, or resource, you create a policy. Which is a document that explicity lists permission.



MFA:

Multifactor authentication (MFA) is a security system that requires more than one method of authentication from independent categories of credentials to verify the user&#39;s identity for a login or other transaction.

#created multi factor autentication.


