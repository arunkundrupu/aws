Loggly:

\* Loggly is a cloud based log management and analytics service provider.

\* Log management as a service i.e., hosted log management platform.

Need: If the servers are increasing and to login to each server for checking the logs is bit difficult i.e., time consuming. So by using loggy we can check the logs of server through the dashboard.

Code deployment with jenkins:

\* Developer writes the code in local machine and push the code in github server and then it reflects the code in jenkins and runs the test cases. If it fails then jenkins sends a notification and developer works on that and again pushes his code to the github repository. If the test case run by the jenkins is success then it deploys the code into the server.

If we use AWS code deploy:

\* The test case run by the jenkins is success then it upload the project file in AWS S3 bucket. AWS code deploy take the latest zip file from AWS S3, extract it and uploads in the server.

\* AWS CodeDeploy is a flexible and reliable deployment configuration and management system that enables you to deploy and update applications running on instances while avoiding downtime. You can view, create, and diagnose applications. Select an application to view details and deploy your application to instances.

VPC:

\* Amazon Virtual Private Cloud is our own network which enables us to launch AWS resources into a virtual network that we have defined.

\* It is logically isolated from other virtual networks in the AWS Cloud.

\* We can launch our AWS resources, such as Amazon EC2 instances, into our VPC. We can specify an IP address range for the VPC, add subnets, associate security groups, and configure route tables.

Subnet:

\* Subnetworks inside the VPC in which each subnet can be one availability zone.

\* A subnet is a range of IP addresses in your VPC.

\* We can launch AWS resources into a specified subnet.

\* Use a public subnet for resources that must be connected to the internet, and a private subnet for resources that won&#39;t be connected to the internet.

Security Groups:

\* Its one way of controlling which access is allowed to instances and which traffic may leave the instances. It acts like a fire wall.

\* To protect the AWS resources in each subnet, you can use multiple layers of security, including security groups and network access control lists (ACL). For more information, see Security.
