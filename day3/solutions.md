

**Amazon Elastic Compute Cloud:**

Amazon EC2 provides scalable computing capacity in the Amazon Web Services (AWS) cloud. Using Amazon EC2 eliminates your need to invest in hardware up front, so you can develop and deploy applications faster. You can use Amazon EC2 to launch as many or as few virtual servers as you need, configure security and networking, and manage storage. Amazon EC2 enables you to scale up or down to handle changes in requirements or spikes in popularity, reducing your need to forecast traffic.

**Features of Amazon EC2:**

- Virtual computing environments, known as _instances._
- Preconfigured templates for your instances, known as Amazon Machine Images (AMIs), that package the bits you need for your server (including the operating system and additional software).
- Various configurations of CPU, memory, storage, and networking capacity for your instances, known as _instance types._
- Secure login information for your instances using key pairs (AWS stores the public key, and we store the private key in a secure place).
- Storage volumes for temporary data that&#39;s deleted when you stop or terminate your instance, known as instance store volumes.
- Persistent storage volumes for your data using Amazon Elastic Block Store known as Amazon EBS volumes.
- Multiple physical locations for your resources, such as instances and Amazon EBS     volumes, known as regions and Availability Zones.
- A firewall that enables you to specify the protocols, ports, and source IP ranges that can reach your instances using security groups.
- Static IPv4 addresses for dynamic cloud computing, known as Elastic IP addresses
- Metadata, known as tags, that you can create and assign     to your Amazon EC2 resources.
- Virtual networks you can create that are logically isolated from the rest of the AWS cloud, and that you can optionally connect to your own network, known as virtual private clouds (VPCs).

**Types of EC2 Instances:**

Instance types comprise varying combinations of CPU, memory, storage, and networking capacity and give you the flexibility to choose the appropriate mix of resources for your applications.

**a) General purpose:**

1. T2 instances are Burstable Performance Instances that provide a baseline level of CPU performance with the ability to burst above the baseline.

**Use Cases**

Websites and web applications, development environments, build servers, code repositories, microservices, test and staging environments, and line of business applications.

2. M5 instances are the latest generation of General Purpose Instances. This family provides a balance of compute, memory, and network resources, and it is a good choice for many applications.

**Use Cases**

Small and mid-size databases, data processing tasks that require additional memory, caching fleets, and for running backend servers for SAP, Microsoft SharePoint, cluster computing, and other enterprise applications.

M4 same as M5.

## **b) Compute Optimized:**

1. C5 instances are optimized for compute-intensive workloads and deliver very cost-effective high performance at a low price per compute ratio.

**Use Cases**

High performance web servers, scientific modelling, batch processing, distributed analytics, high-performance computing (HPC), machine/deep learning inference, ad serving, highly scalable multiplayer gaming, and video encoding.

2. C4 instances are optimized for compute-intensive workloads and deliver very cost-effective high performance at a low price per compute ratio.

**Use Cases**

High performance front-end fleets, web-servers, batch processing, distributed analytics, high performance science and engineering applications, ad serving, MMO gaming, and video-encoding.

## **c) Memory Optimized:**

1. X1e instances are optimized for high-performance databases, in-memory databases and other memory intensive enterprise applications. X1e instances offer one of the lowest price per GiB of RAM among Amazon EC2 instance types.

**Use Cases**

High performance databases, in-memory databases (e.g. SAP HANA) and memory intensive applications. x1e.32xlarge instance certified by SAP to run next-generation Business Suite S/4HANA, Business Suite on HANA (SoH), Business Warehouse on HANA (BW), and Data Mart Solutions on HANA on the AWS cloud.

2. X1 instances are optimized for large-scale, enterprise-class and in-memory applications, and offer one of the lowest price per GiB of RAM among Amazon EC2 instance types.

**Use Cases**

In-memory databases (e.g. SAP HANA), big data processing engines (e.g. Apache Spark or Presto), high performance computing (HPC). Certified by SAP to run Business Warehouse on HANA (BW), Data Mart Solutions on HANA, Business Suite on HANA (SoH), Business Suite S/4HANA.

R4 instances are optimized for memory-intensive applications and offer better price per GiB of RAM than R3.

**Use Cases**

High performance databases, data mining &amp; analysis, in-memory databases, distributed web scale in-memory caches, applications performing real-time processing of unstructured big data, Hadoop/Spark clusters, and other enterprise applications.

## **d) Accelerated Computing:**

1. P3 instances are the latest generation of general purpose GPU instances.

**Use Cases**

Machine/Deep learning, high performance computing, computational fluid dynamics, computational finance, seismic analysis, speech recognition, autonomous vehicles, drug discovery.

2. P2 instances are intended for general-purpose GPU compute applications.

**Use Cases**

Machine learning, high performance databases, computational fluid dynamics, computational finance, seismic analysis, molecular modeling, genomics, rendering, and other server-side GPU compute workloads.

3. G3 instances are optimized for graphics-intensive applications.

**Use Cases**

3D visualizations, graphics-intensive remote workstation, 3D rendering, application streaming, video encoding, and other server-side graphics workloads.

4. F1 instances offer customizable hardware acceleration with field programmable gate arrays (FPGAs).

**Use Cases**

Genomics research, financial analytics, real-time video processing, big data search and analysis, and security.

## **e) Storage Optimized**

1. H1 instances feature up to 16 TB of HDD-based local storage, deliver high disk throughput, and a balance of compute and memory.

**Use Cases**

MapReduce-based workloads, distributed file systems such as HDFS and MapR-FS, network file systems, log or data processing applications such as Apache Kafka, and big data workload clusters.

2. I3 instance family provides Non-Volatile Memory Express (NVMe) SSD-backed instance storage optimized for low latency, very high random I/O performance, high sequential read throughput and provide high IOPS at a low cost.  I3 also offers Bare Metal instances (i3.metal), powered by the Nitro System, for non-virtualized workloads, workloads that benefit from access to physical resources, or workloads that may have license restrictions.

**Use Cases**

NoSQL databases (e.g. Cassandra, MongoDB, Redis), in-memory databases (e.g. Aerospike), scale-out transactional databases, data warehousing, Elasticsearch, analytics workloads.

3. D2 instances feature up to 48 TB of HDD-based local storage, deliver high disk throughput, and offer the lowest price per disk throughput performance on Amazon EC2.

**Use Cases**

Massively Parallel Processing (MPP) data warehousing, MapReduce and Hadoop distributed computing, distributed file systems, network file systems, log or data-processing applications.

**Instance Categories:**

**On-Demand Instances:**

Pay for the instances that you use by the second, with no long-term commitments or upfront payments. With On-Demand instances, you pay for compute capacity by per hour or per second depending on which instances you run. No longer-term commitments or upfront payments are needed. You can increase or decrease your compute capacity depending on the demands of your application and only pay the specified per hourly rates for the instance you use.

**Reserved Instances:**

Make a low, one-time, up-front payment for an instance, reserve it for a one- or three-year term, and pay a significantly lower hourly rate for these instances.

Reserved Instances provide you with a significant discount (up to 75%) compared to On-Demand instance pricing. In addition, when Reserved Instances are assigned to a specific Availability Zone, they provide a capacity reservation, giving you additional confidence in your ability to launch instances when you need them.

**Spot Instances:**

Request unused EC2 instances, which can lower your costs significantly.

### **Dedicated Hosts:**

A Dedicated Host is a physical EC2 server dedicated for your use. Dedicated Hosts can help you reduce costs by allowing you to use your existing server-bound software licenses, including Windows Server, SQL Server, and SUSE Linux Enterprise Server (subject to your license terms), and can also help you meet compliance requirements.

**Tags:**

A tag is a label that you assign to an AWS resource. Each tag consists of a key and an optional value, both of which you define.

The following basic restrictions apply to tags:

- Maximum number of tags per resource – 50
- For each resource, each tag key must be unique, and each tag key can have only     one value.
- Maximum key length – 128 Unicode characters in UTF-8
- Maximum value length – 256 Unicode characters in UTF-8
- If your tagging schema is used across multiple services and resources, remember that other services may have restrictions on allowed characters. Generally allowed characters are: letters, numbers, and spaces representable in UTF-8, and the following characters: + - = . \_ : / @.
- Tag keys and values are case-sensitive.
- Don&#39;t use the aws: prefix for either keys or values; it&#39;s reserved for AWS use. You can&#39;t edit or delete tag keys or values with this prefix. Tags with this prefix do not count against your tags per resource limit.

# You can&#39;t terminate, stop, or delete a resource based solely on its tags. But you must specify the resource identifier.

**Elastic IP:**

An Elastic IP address is a static IPv4 address designed for dynamic cloud computing. An Elastic IP address is associated with your AWS account. With an Elastic IP address, you can mask the failure of an instance or software by rapidly remapping the address to another instance in your account.

- An Elastic IP address is a public IPv4 address, which is reachable from the internet. If your instance does not have a public IPv4 address, you can associate an Elastic IP address with your instance to enable communication with the internet.
- AWS currently do not support Elastic IP addresses for IPv6.
- To use an Elastic IP address, you first allocate one to your account, and then     associate it with your instance or a network interface. When you associate an Elastic IP address with an instance or its primary network interface, the instance&#39;s public IPv4 address (if it had one) is released back into Amazon&#39;s pool of public IPv4 addresses. You cannot reuse a public IPv4 address.
- A disassociated Elastic IP address remains allocated to your account until you explicitly release it.


