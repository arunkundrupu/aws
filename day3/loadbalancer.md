

**Load Balancer:**

# Launched two Amazon Ubuntu instances.

# Installed apache2 on both the machines and restarted the apache server.
# modified index.html file

Instance1:

# vi /var/www/html/index.html

&lt;html&gt;

&lt;h1 style=&quot;background-color:DodgerBlue;&quot;&gt;Hello server&lt;/h1&gt;

&lt;p style=&quot;background-color:Tomato;&quot;&gt;1st server&lt;/p&gt;

&lt;/html&gt;

Instance2:

&lt;html&gt;

&lt;h1 style=&quot;background-color:DodgerRed;&quot;&gt;Hello World&lt;/h1&gt;

&lt;p style=&quot;background-color:Grey;&quot;&gt;second server&lt;/p&gt;

&lt;/html&gt;

# Created classic load balancer

# Attached both the instances to load balancer.

# Now using the Load balancer DNS name checked the load distributed to both the servers one after the other.


