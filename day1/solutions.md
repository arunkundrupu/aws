

### Task 1

Create a vpc through wizard, having one public subnet and one private subnet.

### Task 2

Create two instances within the vpc that you created in task 1, windows instance in public subnet and linux instance in private subnet. check if linux is pingable from windows and vice versa.

# Created VPC

![aws](https://github.com/arunkundrupu1990/AWS/blob/master/day1-images/01%20vpc.png)

# created subnets with one public subnet in one zone second one in other availability zone and two private subnets with one private subnet in one zone second one in other availability zone

![aws](https://github.com/arunkundrupu1990/AWS/blob/master/day1-images/02%20subnets.png)

#created internet gate way and attched to public subnet.

# created NAT gate way and attached to private subnet

![aws](https://github.com/arunkundrupu1990/AWS/blob/master/day1-images/03%20NAT-%20gateway.png)

# launched windows instance in public subnet

![aws](https://github.com/arunkundrupu1990/AWS/blob/master/day1-images/04%20windows-public-subnet.png)

# launched linux(ubuntu) machine in private subnet

![aws](https://github.com/arunkundrupu1990/AWS/blob/master/day1-images/05%20linux-private%20subnet.png)

# logged in to windows machine.

# created one more public linux instance to login to linux (private subnet)

![aws](https://github.com/arunkundrupu1990/AWS/blob/master/day1-images/06%20public-private-connection.png)

#pinged from windows to linux but connection timed out.

![aws](https://github.com/arunkundrupu1990/AWS/blob/master/day1-images/07%20windows-linux-ping.png)


